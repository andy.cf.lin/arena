# Definition for a binary tree node.
from typing import Optional


class TreeNode:
     def __init__(self, val=0, left=None, right=None):
         self.val = val
         self.left = left
         self.right = right

class Solution:
    def maxPathSum(self, root: Optional[TreeNode]) -> int:

        max_path = -float('inf')

        def gain_from_subtree(node: Optional[TreeNode]) -> int:

            nonlocal max_path

            # handle null root case
            if not node:
                return 0
            
            gain_from_left = max(gain_from_subtree(node.left), 0)

            gain_from_right = max(gain_from_subtree(node.right), 0)

            max_path = max(max_path, gain_from_left + gain_from_right + node.val)

            return max( gain_from_left + node.val, gain_from_right + node.val)

        gain_from_subtree(root)
        return max_path
    
'''Plot the input tree:
    -10
    /  \
    9   20
        /  \
      15   7
'''

def main():
    # Your code here
    input = [-10, 9, 20, None, None, 15, 7]

    # convert input from list to TreeNode structure
    root = TreeNode(input[0])   # root node
    root.left = TreeNode(input[1])
    root.right = TreeNode(input[2])
    root.right.left = TreeNode(input[5])
    root.right.right = TreeNode(input[6])
    
    s = Solution()

    output = s.maxPathSum(root)
    print(output)

if __name__ == "__main__":
    main()
