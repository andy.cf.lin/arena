from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        
        min_price = float('inf')
        max_profit = 0
        for i in range(len(prices)):
            if prices[i] < min_price:
                min_price = prices[i]
            elif prices[i] - min_price > max_profit:
                max_profit = prices[i] - min_price
                
        return max_profit


def main():

    s = Solution()
    # Your code here
    input = [7,1,5,3,6,4]

    output = s.maxProfit(input)
    print(output)

if __name__ == "__main__":
    main()
