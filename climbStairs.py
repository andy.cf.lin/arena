class Solution:
    def climbStairs(self, n: int) -> int:
        
        if (n == 1):
            return 1

        sum_list = []
        sum = 0

        for i in range(n+1):
            
            if i <= 2: 
                sum = i
            else:
                sum = sum_list[i-1] + sum_list[i-2]

            sum_list.append(sum)

        return sum


def main():
    # Your code here
    input = 3

    s = Solution()

    output = s.climbStairs(input)
    print(output)

if __name__ == "__main__":
    main()

