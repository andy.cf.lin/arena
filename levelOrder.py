from typing import List, Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
         self.val = val
         self.left = left
         self.right = right

class Solution:
    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:

        # perfrom bfs to traverse through the tree
        
        levels = []
        # handle empty tree
        if not root:
            return levels

        # recursive function for tree traversal
        def helper(node, level):

            # start the current level
            if len(levels) == level:
                levels.append([])

            # append the current node value
            levels[level].append(node.val)

            # process child nodes for the next level
            if node.left:
                helper(node.left, level + 1)
            if node.right:
                helper(node.right, level + 1)

        helper(root, 0)
        return levels


def main():
    # Your code here
    input = [3, 9, 20, None, None, 15, 7]

    # convert input from list to TreeNode structure
    root = TreeNode(input[0])   # root node
    root.left = TreeNode(input[1])
    root.right = TreeNode(input[2])
    root.right.left = TreeNode(input[5])
    root.right.right = TreeNode(input[6])
    
    s = Solution()

    output = s.levelOrder(root)
    print(output)

if __name__ == "__main__":
    main()
