
from typing import List
from itertools import permutations

class Solution:

#    def permutations(elements):
#
#        if len(elements) <= 1:
#            yield elements 
#            return
#        for perm in permutations(elements[1:]):
#            for i in range(len(elements)):
#                yield perm[:i] + elements[0:1] + perm[i:]

    def perm_approach(self, nums: List[int]) -> List[List[int]]:
        
        #step 1: generate all permutations

        perm_set = permutations(nums, 3)

        result = []

        #print("Perm: {}".format(perm_set))

        #step 2: given permutations in the list, check if the num equals zero
        for perm in perm_set:
            
            sum = 0
            for el in perm:
                sum += el
            
            if sum == 0:
                result.append(sorted(perm))
                            
            
        return list(set(tuple(i) for i in result))  
    
    def bruteforce_apprach(self, nums: List[int]) -> List[List[int]]:

        result = []

        for i in range(len(nums)):
            j = i+1; k = len(nums)-1
            while j < k:
                sum = nums[i] + nums[j] + nums[k]
                if sum == 0:
                    result.append(sorted([nums[i], nums[j], nums[k]]))
                                    
                    j += 1
                    k -= 1
                elif sum < 0: j+=1
                else: k-=1

        return list(set(tuple(i) for i in result))


    def threeSum(self, nums: List[int]) -> List[List[int]]:

        return self.bruteforce_apprach(sorted(nums))
                    
# create a main function to test the 3sum function
def main():

    # create a list of numbers
    input = [-1,0,1,2,-1,-4]

    s = Solution()
    output = s.threeSum(input)
    print ("Result: {}".format(output))

if __name__ == "__main__":
    main()